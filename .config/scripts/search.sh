#!/usr/bin/env bash

# Defining our web browser.
#DMBROWSER="vivaldi"
DMBROWSER="brave"
DMARGS=""
while getopts 'pn' OPTION; do
    case "$OPTION" in
        p)
           DMARGS="--incognito"
           ;;
        n)
           DMARGS="--new-window"
            ;;
    esac
done

# An array of search engines.  You can edit this list to add/remove
# search engines. The format must be: "engine_name]="url".
# The url format must allow for the search keywords at the end of the url.
# For example: https://www.amazon.com/s?k=XXXX searches Amazon for 'XXXX'.
declare -A options
options[amazon]="https://www.amazon.de/s?k="
options[archwiki]="https://wiki.archlinux.org/index.php?search="
#options[bbcnews]="https://www.bbc.co.uk/search?q="
#options[cnn]="https://www.cnn.com/search?q="
options[ccpt]="https://chat.openai.com/?q="
options[duckduckgo]="https://duckduckgo.com/?q="
options[Brave-Search]="https://search.brave.com/search?q="
options[englisch]="https://dict.leo.org/german-english/"
#options[gitlab]="https://gitlab.com/search?search="
options[fcpt]="https://kagi.com/fastgpt?query="
options[google]="https://www.google.com/search?q="
options[imdb]="https://www.imdb.com/find?q="
options[italienisch]="https://dict.leo.org/italienisch-deutsch/"
options[kagi]="https://kagi.com/search?q="
options[nixos]="https://search.nixos.org/packages?channel=23.11&from=0&size=50&sort=relevance&type=packages&query="
options[reddit]="https://www.reddit.com/search/?q="
options[stack]="https://stackoverflow.com/search?q="
options[startpage]="https://www.startpage.com/do/dsearch?query="
options[wikipedia]="https://en.wikipedia.org/wiki/"
#options[wolfram]="https://www.wolframalpha.com/input/?i="
#options[youtube]="https://www.youtube.com/results?search_query="

# Picking a search engine.
while [ -z "$engine" ]; do
    engine=$(printf '%s\n' "${!options[@]}" | sort | dmenu -i -l 20 -p 'Choose search engine:') || exit
    url=$(echo "${options["${engine}"]}") || exit
done

# Searching the chosen engine.
while [ -z "$query" ]; do
    query=$(echo "$engine" | dmenu -p 'Enter search query:') || exit
done

# Display search results in web browser
$DMBROWSER $DMARGS "$url""$query"

