#!/bin/sh

chosen=$(printf "Shutdown\nReboot\nSuspend" | dmenu -i -p "Poweroptions")

if [ "$chosen" = "Recompile" ]; then
	xmonad --recompile && alacritty -e less ~/.xmonad/xmonad.errors
elif [ "$chosen" = "Restart" ]; then
	xmonad --restart
elif [ "$chosen" = "Reboot" ]; then
	systemctl reboot
elif [ "$chosen" = "Shutdown" ]; then
	systemctl poweroff
elif [ "$chosen" = "Suspend" ]; then
	systemctl suspend
fi
