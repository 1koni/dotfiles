#!/bin/sh

chosen=$(printf "Disable\n60\n55\n50\n45\n40\n35\n30\n25\n20\n15\n10" | fuzzel -d -l 12 --width 30 -p "Blue Light Filter: ")

if [ "$chosen" = "Disable" ]; then
	killall hyprsunset
elif [ "$chosen" = "60" ]; then
	hyprsunset -t 6000 &
elif [ "$chosen" = "55" ]; then
	hyprsunset -t 5500 &
elif [ "$chosen" = "50" ]; then
	hyprsunset -t 5000 &
elif [ "$chosen" = "45" ]; then
	hyprsunset -t 4500 &
elif [ "$chosen" = "40" ]; then
	hyprsunset -t 4000 &
elif [ "$chosen" = "35" ]; then
	hyprsunset -t 3500 &
elif [ "$chosen" = "30" ]; then
	hyprsunset -t 3000 &
elif [ "$chosen" = "25" ]; then
	hyprsunset -t 2500 &
elif [ "$chosen" = "20" ]; then
	hyprsunset -t 2000 &
elif [ "$chosen" = "15" ]; then
	hyprsunset -t 1500 &
elif [ "$chosen" = "10" ]; then
	hyprsunset -t 1000 &
fi
