#!/bin/sh

source="$(pactl list short sinks | cut -f 2 | grep -v "hdmi" | fuzzel -d -l 5 -p "Change Audio Output: ")";
# inputs="$(pactl list sink-inputs short | cut -f 1)"

# for input in $inputs; do
# 	pactl move-sink-input "$input" "$source";
# done

pactl set-default-sink "$source";
