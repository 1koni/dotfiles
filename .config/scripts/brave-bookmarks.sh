#!/bin/env bash

# Default Brave bookmarks file location
BRAVE_BOOKMARKS=$HOME/.config/BraveSoftware/Brave-Browser/Default/Bookmarks

# Use jq to filter the URLs and pipe into Rofi
url=$(jq -cr '.roots[]?.children[]?.children[]? | {url} | join(" 🌐 ")' "$BRAVE_BOOKMARKS" | sort -u | fuzzel -d --width 170 -l 40 -p 'Lesezeichen: ')

# If user doesn't pick anything, exit
if [[ -z "${url// }" ]]; then
  exit 1
fi

# Launch Brave browser and feed it the selected URL from Rofi
brave "$url"
