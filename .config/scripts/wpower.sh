#!/bin/sh

chosen=$(printf "Shutdown\nLogout\nReboot\nSuspend" | fuzzel -l 4 -d -p "Poweroptions: ")

if [ "$chosen" = "Logout" ]; then
	hyprctl dispatch exit 1
elif [ "$chosen" = "Reboot" ]; then
	systemctl reboot
elif [ "$chosen" = "Shutdown" ]; then
	systemctl poweroff
elif [ "$chosen" = "Suspend" ]; then
	systemctl suspend
fi
