#!/bin/sh

while getopts "fl" OPTION; do
    case "$OPTION" in
        l)
            BROWSER="floorp"
		        cp "$HOME/.floorp/"*.default-release"/places.sqlite" "/tmp/places.sqlite"
            ;;
        f)
            BROWSER="firefox"
		        cp "$HOME/.mozilla/firefox/"*.default-release"/places.sqlite" "/tmp/places.sqlite"
            ;;
        *) exit 1
            ;;
    esac
done

get_bookmarks() {
    sqlite3 "/tmp/places.sqlite" "
        SELECT b.title, p.url 
        FROM moz_bookmarks b 
        JOIN moz_places p ON b.fk = p.id 
        WHERE b.type = 1 AND b.title IS NOT NULL 
				ORDER BY b.title ASC;
    " | sed 's/|/\t/'
    
    # Clean up
    rm "/tmp/places.sqlite"
}

selected=$(get_bookmarks | fuzzel -d -l 35 --width 70 -p "Select Bookmark: " | cut -f2)

if [ -n "$selected" ]; then
    "$BROWSER" "$selected"
fi
