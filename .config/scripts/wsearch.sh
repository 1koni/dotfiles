#!/usr/bin/env bash

# Defining our web browser.
#DMBROWSER="brave"
DMBROWSER="floorp"
DMARGS=""
while getopts "pn" OPTION; do
    case "$OPTION" in
        p)
            DMARGS="--private-window"
            # DMARGS="--incognito"
            ;;
        n)
            DMARGS="--new-window"
            ;;
	*) exit 1
	   ;;
    esac
done

# An array of search engines.  You can edit this list to add/remove
# search engines. The format must be: "engine_name]="url".
# The url format must allow for the search keywords at the end of the url.
# For example: https://www.amazon.com/s?k=XXXX searches Amazon for 'XXXX'.
declare -A options
options[Amazon]="https://www.amazon.de/s?k="
options[Archwiki]="https://wiki.archlinux.org/index.php?search="
#options[arxiv]="https://arxiv.org/search/?searchtype=all&source=header&query="
#options[bbcnews]="https://www.bbc.co.uk/search?q="
#options[cnn]="https://www.cnn.com/search?q="
options["ChatCPT (ccpt)"]="https://chat.openai.com/?q="
options[Duckduckgo]="https://duckduckgo.com/?q="
options[Brave-Search]="https://search.brave.com/search?q="
options[Englisch]="https://dict.leo.org/german-english/"
#options[gitlab]="https://gitlab.com/search?search="
options["FastCPT (fcpt)"]="https://kagi.com/fastgpt?query="
options[Google]="https://www.google.com/search?q="
options[Imdb]="https://www.imdb.com/find?q="
options[Italienisch]="https://dict.leo.org/italienisch-deutsch/"
options[Kagi]="https://kagi.com/search?q="
options[NixOS]="https://search.nixos.org/packages?channel=24.11&from=0&size=50&sort=relevance&type=packages&query="
options[Reddit]="https://www.reddit.com/search/?q="
options[Stackoverflow]="https://stackoverflow.com/search?q="
options[Startpage]="https://www.startpage.com/do/dsearch?query="
#options[thesaurus]="https://www.thesaurus.com/misspelling?term="
options[Wikipedia En]="https://en.wikipedia.org/wiki/"
options[Wikipedia De]="https://de.wikipedia.org/wiki/"
#options[wiktionary]="https://de.wiktionary.org/wiki/"
#options[wolfram]="https://www.wolframalpha.com/input/?i="
#options[youtube]="https://www.youtube.com/results?search_query="

# Picking a search engine.
while [ -z "$engine" ]; do
    engine=$(printf '%s\n' "${!options[@]}" | sort | fuzzel -d -l 17 -p 'Choose search engine: ')|| exit
    url="${options["${engine}"]}" || exit
done

# Searching the chosen engine.
while [ -z "$query" ]; do
    query=$(fuzzel -w 100 -d --prompt-only "$engine"": " )|| exit
done

# Display search results in web browser
$DMBROWSER $DMARGS "$url""$query"

