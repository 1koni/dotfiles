#!/bin/sh

chosen=$(printf "Recompile\nRestart\nErrors\nKeys" | dmenu -i -p "XMonad Options")

if [ "$chosen" = "Recompile" ]; then
	xmonad --recompile && alacritty -e less ~/.xmonad/xmonad.errors
elif [ "$chosen" = "Restart" ]; then
	xmonad --restart
elif [ "$chosen" = "Errors" ]; then
	alacritty -e less ~/.xmonad/xmonad.errors
elif [ "$chosen" = "Keys" ]; then
	alacritty -e $HOME/.config/scripts/keys.sh
fi
