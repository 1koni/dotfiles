;test
;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

;; NOTE: If you want to move everything out of the ~/.emacs.d folder
;; reliably, set `user-emacs-directory` before loading no-littering!
;(setq user-emacs-directory "~/.cache/emacs")

(use-package no-littering)

;; no-littering doesn't set this by default so we must place
;; auto save files in the same path as it uses for sessions
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
(setq backup-directory-alist `(("." . "~/.saves")))

(use-package undo-fu-session)

(global-undo-fu-session-mode)

(setq inhibit-startup-message t)

(scroll-bar-mode -1)		; Disable visible scrollbar
(tool-bar-mode -1)			; Disable the toolbar
(tooltip-mode -1)				; Disable tooltips
;(set-fringe-mode 10)		; Give some breathing room
(menu-bar-mode -1)			; Disable the menu bar

(setq visible-bell t)		; Setup visible-bell

(column-number-mode)
(global-display-line-numbers-mode t)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
		 treemacs-mode-hook
                shell-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defun efs/set-font-faces ()
      (message "Setting faces!")
      (set-face-attribute 'default nil :font "Fira Code Retina" :height 110)

      ;; Set the fixed pitch face
      (set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 100)

      ;; Set the variable pitch face
      (set-face-attribute 'variable-pitch nil :font "Cantarell" :height 120 :weight 'regular))

    (if (daemonp)
        (add-hook 'after-make-frame-functions
                  (lambda (frame)
                    (setq doom-modeline-icon t)
                    (with-selected-frame frame
                      (efs/set-font-faces))))
        (efs/set-font-faces))

  ;
  ;  ;Font Configuration ............................
  ;
  ;  (set-face-attribute 'default nil :font "Fira Code Retina" :height 130)
  ;
  ;  ;; Set the fixed pitch face
  ;  (set-face-attribute 'fixed-pitch nil :font "Fira Code Retina" :height 120)
  ;
  ;  ;; Set the variable pitch face
  ;  (set-face-attribute 'variable-pitch nil :font "Cantarell" :height 140 :weight 'regular)
(use-package beacon)
(beacon-mode 1)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-M-j") 'counsel-switch-buffer)
(global-set-key (kbd "C-M-a") 'org-agenda)
(global-set-key (kbd "C-M-q") 'org-capture)

(use-package general
  :config
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")

  (rune/leader-keys
    "t"  '(:ignore t :which-key "toggles")
    "tt" '(counsel-load-theme :which-key "choose theme")))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(rune/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))

(require 'recentf)
(setq recentf-auto-cleanup 'never)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

(defun recentd-track-opened-file ()
"Insert the name of the directory just opened into the recent list."
(and (derived-mode-p 'dired-mode) default-directory
     (recentf-add-file default-directory))
;; Must return nil because it is run from `write-file-functions'.
nil)

(defun recentd-track-closed-file ()
  "Update the recent list when a dired buffer is killed.
That is, remove a non kept dired from the recent list."
  (and (derived-mode-p 'dired-mode) default-directory
       (recentf-remove-if-non-kept default-directory)))

(add-hook 'dired-after-readin-hook 'recentd-track-opened-file)
(add-hook 'kill-buffer-hook 'recentd-track-closed-file)

(setq history-lenght 25)
(savehist-mode 1)

(save-place-mode 1)
(global-auto-revert-mode 1)

(use-package dired
    :ensure nil
    :commands (dired dired-jump)
    :bind (("C-x C-j" . dired-jump))
    :custom ((dired-listing-switches "-agho --group-directories-first"))
    :config
    (evil-collection-define-key 'normal 'dired-mode-map
      "h" 'dired-single-up-directory
      "l" 'dired-single-buffer))

  (use-package dired-single)

  (use-package all-the-icons-dired
    :hook (dired-mode . all-the-icons-dired-mode))

  (use-package dired-open
    :config
    ;; Doesn't work as expected!
    ;;(add-to-list 'dired-open-functions #'dired-open-xdg t)
    (setq dired-open-extensions '(("mkv" . "mpv"))))

  (use-package dired-hide-dotfiles
;    :hook (dired-mode . dired-hide-dotfiles-mode)
    :config
    (evil-collection-define-key 'normal 'dired-mode-map
      "s" 'dired-hide-dotfiles-mode))

  (setq global-auto-revert-non-file-buffers t)

(use-package command-log-mode)

(use-package doom-themes
  :init (load-theme 'doom-palenight t))

(use-package all-the-icons)

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
	:custom ((doom-modeline-height 15)))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package smex)
(use-package ivy
 ; :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-apt-done)	
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(defun efs/org-font-setup ()
;  ;; Replace list hyphen with dot
;  (font-lock-add-keywords 'org-mode
;                          '(("^ *\\([-]\\) "
;                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 0)
  (visual-line-mode 1))

(use-package org
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾"
        org-hide-emphasis-markers t)

  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  (setq org-agenda-files
        '("~/Organization/Emacs/Calendar/Birthdays.org"
          "~/Organization/Emacs/Calendar/Tasks.org"
          "~/Organization/Emacs/Calendar/Habits.org"))

(setq org-clock-sound "~/Media/Music/Sounds/small-bell-ring-01a.wav")

(require 'org-habit)
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60)

  (setq org-todo-keywords
    '((sequence "TODO(t)" "next(n)" "|" "DONE(d!)")))

  (setq org-refile-targets
    '(("Archive.org" :maxlevel . 1)
      ("Tasks.org" :maxlevel . 1)))

  ;; Save Org buffers after refiling!
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  (setq org-tag-alist
    '((:startgroup)
       ; Put mutually exclusive tags here
       (:endgroup)
       ("@errand" . ?E)
       ("@home" . ?H)
       ("@work" . ?W)
       ("agenda" . ?a)
       ("planning" . ?p)
       ;("publish" . ?P)
       ("batch" . ?b)
       ("note" . ?n)
       ("idea" . ?i)))

  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
   '(("d" "Dashboard"
     ((agenda "" ((org-deadline-warning-days 14)))
      (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))

    ("W" "Work Tasks" tags-todo "+work-email")

    ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<16&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))))

  (setq org-capture-templates
    `(("t" "Tasks / Projects")
      ("tt" "Task" entry (file+olp "~/Organization/Emacs/Calendar/Tasks.org" "Inbox")
           "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

      ("j" "Journal Entries")
      ("jj" "Journal" entry
           (file+olp+datetree "~/Organization/Emacs/Calendar/Journal.org")
           "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
           ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
           :clock-in :clock-resume
           :empty-lines 1)
      ("jm" "Meeting" entry
           (file+olp+datetree "~/Organization/Emacs/Calendar/Journal.org")
           "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
           :clock-in :clock-resume
           :empty-lines 1)

      ("w" "Workflows")
      ("we" "Checking Email" entry (file+olp+datetree "~/Organization/Emacs/Calendar/Journal.org")
           "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)))

  (efs/org-font-setup))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))

(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t)
;    (cpp . t)
    (python . t)))

(setq org-confirm-babel-evaluate nil)

 (require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
;(add-to-list 'org-structure-template-alist '("cp" . "src cpp"))

(push '("conf-unix" . conf-unix) org-src-lang-modes)

(defun efs/org-babel-tangle-config ()
  (when (string-equal (buffer-file-name)
                      (expand-file-name "~/.dotfiles/.emacs.d/Emacs.org"))
    ;; Dynamic scoping to the rescue
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/Organization/Emacs/OrgRoam")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i"    . completion-at-point)
         :map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies) ;; Ensure the keymap is available
  (org-roam-db-autosync-mode)
  (org-roam-setup))

(use-package highlight-indent-guides
  :hook (c-mode . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-method 'bitmap))

(use-package display-fill-column-indicator
  :hook (c-mode . display-fill-column-indicator-mode)
  :custom
  (display-fill-column-indicator-column '120))

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(defun efs/lsp-mode-setup ()
       (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
       (lsp-headerline-breadcrumb-mode))

       (use-package lsp-mode
         :commands (lsp lsp-deferred)
         :hook (lsp-mode . efs/lsp-mode-setup)
         :hook (c-mode . lsp)
         :hook (c++-mode . lsp)
         :init
         (setq lsp-keymap-prefix "C-l")  ;; Or 'C-l', 's-l'
         :config
         (lsp-enable-which-key-integration t))

   (require 'lsp)
   (require 'lsp-haskell)
 ;  ;; Hooks so haskell and literate haskell major modes trigger LSP setup
   (add-hook 'haskell-mode-hook #'lsp)
   (add-hook 'haskell-literate-mode-hook #'lsp)

;   (add-hook 'c-mode-hook #'lsp)

     (use-package lsp-ui
       :hook (lsp-mode . lsp-ui-mode)
       :custom
       (lsp-ui-doc-position 'bottom))

     (use-package lsp-treemacs
       :after lsp)

     (use-package lsp-ivy)

     (require 'eglot)
     (add-to-list 'eglot-server-programs '((c++-mode c-mode) "clangd"))
     (add-hook 'c-mode-hook 'eglot-ensure)
     (add-hook 'c++-mode-hook 'eglot-ensure)

(use-package dap-mode
;; Uncomment the config below if you want all UI panes to be hidden by default!
 :custom
 (lsp-enable-dap-auto-configure nil)
 :config
 (dap-ui-mode 1)

:config
;; Set up Node debugging
(require 'dap-node)
;;(dap-node-setup) ;; Automatically installs Node debug adapter if needed

;; Bind `C-c l d` to `dap-hydra` for easy access
(general-define-key
  :keymaps 'lsp-mode-map
  :prefix lsp-keymap-prefix
  "d" '(dap-hydra t :wk "debugger")))

(use-package python-mode
  :ensure t
  :hook (python-mode . lsp-deferred)
  :custom
  ;; NOTE: Set these if Python 3 is called "python3" on your system!
  ;; (python-shell-interpreter "python3")
  ;; (dap-python-executable "python3")
  (dap-python-debugger 'debugpy)
  :config
  (require 'dap-python))

(use-package pyvenv
  :config
  (pyvenv-mode 1))

(setq c-default-style "linux"
      c-basic-offset 2)

(use-package company
    :after lsp-mode
    :hook (lsp-mode . company-mode)
    :bind (:map company-active-map
           ("<tab>" . company-complete-selection))
          (:map lsp-mode-map
           ("<tab>" . company-indent-or-complete-common))
    :custom
    (company-minimum-prefix-length 1)
    (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/dotfilespc")
    (setq projectile-project-search-path '("~/dotfilespc")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(add-hook 'magit-process-find-password-functions
          'magit-process-password-auth-source)

;; NOTE: Make sure to configure a GitHub token before using this package!
;; - https://magit.vc/manual/forge/Token-Creation.html#Token-Creation
;; - https://magit.vc/manual/ghub/Getting-Started.html#Getting-Started
;(use-package forge) -> für bessere git Interaktion, z.B. requests, Fragen etc.

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;     (use-package elfeed)
;     (use-package elfeed-goodies)
;     (elfeed-goodies/setup)
;  ;   (setq elfeed-goodies/entry-pane-size 0.6)
;   (evil-define-key 'normal elfeed-show-mode-map
;   (kbd "J") 'elfeed-goodies/split-show-next
;   (kbd "K") 'elfeed-goodies/split-show-prev)
;   (setq elfeed-goodies/entry-pane-position 'bottom)
;        (setq elfeed-feeds 
;              '(("https://www.reddit.com/r/Austria.rss" reddit Österreich)
;              ("https://www.reddit.com/r/graz.rss" reddit Österreich)
;              ("https://www.reddit.com/r/austriansports.rss" reddit Österreich Sport)
;      ;        ("https://www.cio.com/category/linux/index.rss" Linux)
;      ;        ("https://www.reddit.com/r/emacs.rss" Linux Emacs reddit)
;      ;        ("https://www.reddit.com/r/linux.rss" Linux reddit)
;      ;        ("https://archlinux.org/feeds/news/" Linux)
;              ("https://www.reddit.com/r/news.rss" reddit Nachrichten)))

(use-package mu4e
  :ensure nil
  ;:defer 20 ; Wait until 20 seconds after startup
  :bind (("C-x C-m" . mu4e))
  :config

  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)
  (let ((private-file (expand-file-name "private.el" user-emacs-directory)))
    (when (file-exists-p private-file)
      (load-file private-file)))

;; Configure the function to use for sending mail
  (setq message-send-mail-function 'smtpmail-send-it)
  (setq mu4e-compose-format-flowed t)

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "~/Organization/Mail")

  (setq smtpmail-smtp-server "smtp.mailbox.org"
        smtpmail-smtp-service 587 
        smtpmail-stream-type  'starttls)

  (setq mu4e-drafts-folder "/Drafts")
  (setq mu4e-sent-folder   "/Sent")
  (setq mu4e-refile-folder "/Archive")
  (setq mu4e-trash-folder  "/Trash")

(setq mu4e-maildir-shortcuts
  '((:maildir "/Inbox"          :key ?i)
    (:maildir "/Sent"           :key ?s)
    (:maildir "/Trash"          :key ?t)
    (:maildir "/Drafts"         :key ?d)
    (:maildir "/Bank"           :key ?a)
    (:maildir "/Bestellungen"   :key ?b)
    (:maildir "/Nachrichten"    :key ?n)
    (:maildir "/(Spam)"         :key ?p)
    (:maildir "/Archive"        :key ?r))))

(use-package org-mime
    :ensure t)

(setq org-mime-export-options '(:section-numbers nil
                                :with-author nil
                                :with-toc nil))

(add-hook 'message-send-hook 'org-mime-confirm-when-no-multipart)

(defun do-nothing () (interactive))
(global-set-key (kbd "<XF86AudioLowerVolume>") 'do-nothing)
